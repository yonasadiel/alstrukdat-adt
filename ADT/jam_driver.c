#include "stdio.h"
#include "jam.h"

int main() {
	JAM j1, j2;
	long d1, d2;

	printf("------ Jam 1 ------\n");
	printf("Masukan Jam:\n");
	BacaJAM(&j1);
	d1 = JAMToDetik(j1);
	printf("Jumlah detik : %ld\n", d1);
	printf("\n");

	printf("------ Jam 2 -------\n");
	printf("Masukkan detik : "); scanf("%ld", &d2);
	j2 = DetikToJAM(d2);
	printf("Dalam jam : "); TulisJAM(j2); printf("\n");
	printf("\n");

	printf("------ Proses ------\n");
	if (JEQ (j1, j2)) { printf("jam 1 sama dengan jam 2\n");       }
	if (JNEQ(j1, j2)) { printf("jam 1 tidak sama dengan jam 2\n"); }
	if (JLT (j1, j2)) { printf("jam 1 kurang dari jam 2\n");       }
	if (JGT (j1, j2)) { printf("jam 1 lebih dari jam 2\n");        }
	printf("\n");

	printf("jam 1 lebih     1 detik : "); TulisJAM(NextDetik (j1));       printf("\n");
	printf("jam 1 lebih  1000 detik : "); TulisJAM(NextNDetik(j1, 1000)); printf("\n");
	printf("jam 1 kurang    1 detik : "); TulisJAM(PrevDetik (j1));       printf("\n");
	printf("jam 1 kurang 1000 detik : "); TulisJAM(PrevNDetik(j1, 1000)); printf("\n");
	printf("\n");

	printf("Selisih jam 2 dari jam 1 : %ld\n", Durasi(j1, j2));
}