#include "stdio.h"
#include "array.h"

int main() {
	
	TabInt T1, T2;
	ElType X;

	BacaIsi(&T1);
	TulisIsi(T1);

	BacaIsiTab(&T2);
	TulisIsiTab(T2); printf("\n");

	AddAsLastEl(&T2, 10);
	TulisIsiTab(T2); printf("\n");

	AddEli(&T2, 101, 2);
	TulisIsiTab(T2); printf("\n");

	InsSortAsc(&T2);
	TulisIsiTab(T2); printf("\n");

	DelLastEl(&T2, &X);
	TulisIsiTab(T2); printf("\n");

	DelEli(&T2, 2, &X);
	TulisIsiTab(T2); printf("\n");

	printf("max %d min %d\n", MaxUrut(T2), MinUrut(T2));

	Add1Urut(&T2, 100);
	TulisIsiTab(T2); printf("\n");

	printf("%d\n", IsLess(T1, T2));

	return 0;
}