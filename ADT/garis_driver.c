#include "stdio.h"
#include "garis.h"

int main() {
  GARIS G1, G2;

  printf("Masukan Garis 1:\n"); BacaGARIS(&G1);
  printf("Masukan Garis 2:\n"); BacaGARIS(&G2);
  printf("\n");

  printf("--- Garis 1 ---\n");
  printf("Koordinat ujung  : ");    TulisGARIS(G1);                            printf("\n");
  printf("Panjang garis    : %.4f", PanjangGARIS(G1));                         printf("\n");
  printf("Gradien garis    : %.4f", Gradien(G1));                              printf("\n");
  printf("Geser (1.0,2.0)  : ");    GeserGARIS(&G1, 1.0, 2.0); TulisGARIS(G1); printf("\n");
  printf("\n");

  printf("--- Garis 2 ---\n");
  printf("Koordinat ujung  : ");    TulisGARIS(G2);                            printf("\n");
  printf("Panjang garis    : %.4f", PanjangGARIS(G2));                         printf("\n");
  printf("Gradien garis    : %.4f", Gradien(G2));                              printf("\n");
  printf("Geser (2.0,3.0)  : ");    GeserGARIS(&G2, 2.0, 3.0); TulisGARIS(G2); printf("\n");
  printf("\n");

  if (IsTegakLurus(G1, G2)) { printf("Garis 1 dan Garis 2 tegak lurus\n"); }
  if (IsSejajar   (G1, G2)) { printf("Garis 1 dan Garis 2 sejajar\n"    ); }
}