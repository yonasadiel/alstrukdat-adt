#include "stdio.h"
#include "point.h"

int main() {
	POINT P1, P2;

	printf("Masukkan P1, dipisahkan dengan spasi: "); BacaPOINT(&P1);
	printf("Masukkan P2, dipisahkan dengan spasi: "); BacaPOINT(&P2);

	printf("Point yang dimasukkan : "); TulisPOINT(P1); printf(" dan "); TulisPOINT(P2); printf("\n");

	if ( EQ(P1, P2)) { printf("Point P1 dan P2 sama\n"); }
	if (NEQ(P1, P2)) { printf("Point P1 dan P2 beda\n"); }

	if      (IsOrigin(P1)) { printf("P1 adalah Origin\n"); }
	else if (IsOnSbX (P1)) { printf("P1 ada di sumbu X\n"); }
	else if (IsOnSbY (P1)) { printf("P1 ada di sumbu Y\n"); }
	else                   { printf("P1 ada di kuadran %d\n", Kuadran(P1)); }

	if      (IsOrigin(P2)) { printf("P2 adalah Origin\n"); }
	else if (IsOnSbX (P2)) { printf("P2 ada di sumbu X\n"); }
	else if (IsOnSbY (P2)) { printf("P2 ada di sumbu Y\n"); }
	else                   { printf("P2 ada di kuadran %d\n", Kuadran(P2)); }
	printf("\n");

	printf("Pada P1 :\n");
	printf("Next X              : "); TulisPOINT(NextX(P1));                     printf("\n");
	printf("Next Y              : "); TulisPOINT(NextY(P1));                     printf("\n");
	printf("Translasi (5.3,2.7) : "); TulisPOINT(PlusDelta(P1, 5.3, 2.7));       printf("\n");
	printf("Cerminan Sumbu X    : "); TulisPOINT(MirrorOf(P1, true ));           printf("\n");
	printf("Cerminan Sumbu Y    : "); TulisPOINT(MirrorOf(P1, false));           printf("\n");
	printf("Jarak dari origin   : %f", Jarak0(P1));                              printf("\n");
	printf("Jarak ke point kedua: %f", Panjang(P1, P2));                         printf("\n");
	printf("Geser (5.3, 2.7)    : "); Geser(&P1, 5.3, 2.7); TulisPOINT(P1);      printf("\n");
	printf("Mencerminkan Sumbu X: "); Mirror(&P1, false); TulisPOINT(P1);        printf("\n");
	printf("Mencerminkan Sumbu Y: "); Mirror(&P1, true ); TulisPOINT(P1);        printf("\n");
	printf("Putar 90 derajat    : "); Putar(&P1, 90); TulisPOINT(P1);            printf("\n");
	printf("Geser ke Sumbu X    : "); GeserKeSbX(&P1); TulisPOINT(P1);           printf("\n");
	printf("Geser ke Sumbu Y    : "); GeserKeSbY(&P1); TulisPOINT(P1);           printf("\n");

}